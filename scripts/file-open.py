#! /usr/bin/python3
from __future__ import print_function
import sys, subprocess, os, traceback

import gi # sudo apt install python-gobject
gi.require_version('Notify', '0.7')
from gi.repository import Notify

def libnotify(msg):
    # sudo apt install libnotify-bin
    Notify.init("file-open")
    Notify.Notification.new("file-open", msg, "dialog-information").show()
    return

try:
    args = sys.argv[sys.argv.index(__file__)+1:]
    
    fpath = args[0].replace(' ', r'\ ')
    ext = '' if '.' not in fpath else fpath[len(fpath)-fpath[::-1].index('.'):]
    superuser = (os.getuid()==0)

    #access = subprocess.check_output("ls -ld "+fpath,shell=True).decode('utf8').split(' ')
    #is_exec = 'x' in access[0]
    is_exec = False

    if is_exec:
        if access[2]!='root': # You probably can
            prog = None
        else: # You can't
            prog = 'gksudo'
    elif ext=='ipynb':
        prog = 'nbopen'
    elif ext=='jar':
        prog = 'java -jar'
    else:
        prog = 'geany' if not superuser else 'gedit'

    cmd = (prog+' ' if prog else '') + fpath
    libnotify(cmd)
    print('Running:\n%s'%cmd)
    subprocess.check_output(cmd,shell=True,stderr=subprocess.STDOUT)
except Exception:
    libnotify(traceback.format_exc())
    raise
