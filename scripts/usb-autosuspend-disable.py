#! /bin/python3
import sys, os
from subprocess import check_output

def run(cmd): check_output(cmd, shell=True)
def notify(msg):
    msg = msg.replace(" ", "\ ")
    cmd = "notify-send %s" % msg
    if(os.getuid()==0):
        cmd = 'su carlos -c "%s"' % cmd
    run(cmd)

if(os.getuid()!=0):
    # Force to be run as sudo
    run("gksu python3 %s" % os.path.abspath(sys.argv[0]))
else:
    print("sudo granted!")
    run("echo 2 | tee /sys/bus/usb/devices/*/power/autosuspend >/dev/null")
    run("echo on | tee /sys/bus/usb/devices/*/power/level >/dev/null")
    print('Disabled USB AutoSuspend')
    
