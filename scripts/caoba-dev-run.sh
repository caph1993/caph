guake --show
#guake

guake -n ~/caoba/PrivacyEngine/pe_services
guake --rename-current-tab="Back"
guake -e 'sh privacy_engine_ws.sh'

guake -n ~/caoba/PrivacyEngine/pe_services
guake --rename-current-tab="Session"
guake -e 'python3 session_manager_ws.py'

guake -n ~/caoba/Presentation
guake --rename-current-tab="Front"
guake -e 'npm start'

guake -n ~
guake --rename-current-tab="ipy3"
guake -e "ipython3"

# Focus «pe_services» tab
guake -s -3
guake --show
