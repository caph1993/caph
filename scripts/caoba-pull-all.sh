export ANONYLITICS="/home/carlos/caoba"
export GITPULL="/bin/bash -c '/usr/bin/git pull && /usr/bin/git status ; exec /bin/bash -i'" # Git-pulls without closing the terminal
eval "$BASH_POST_RC"

gnome-terminal \
 --tab --working-directory="$ANONYLITICS/Presentation/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/PrivacyEngine/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/Anonymization/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/FileManagement/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/ProjectManagement/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/DFSConnection/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/Interfaces/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/TriggerSparkEngine/" -e "$GITPULL" \
 --tab --working-directory="$ANONYLITICS/MondrianApproachesArticle/" -e "$GITPULL" \
