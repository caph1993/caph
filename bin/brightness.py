#! /usr/bin/python3
"""
Usage:
  brightness.py set <value>
  brightness.py increase <value>
  brightness.py decrease <value>
  brightness.py get
  brightness.py redshift
  brightness.py (-h | --help)
This program changes the artificial brightness of your screen.
Brightness values are in percentage [0,100].

Options:
  -h --help       Show this message.
  set             Sets brightness to value between 0 and 100.
  increase        Increases brightness by the specified amount. (Possibly negative for decreasing)
  decrease        Decreases brightness by the specified amount. (Possibly negative for increasing)
  get             Prints the current brightness level.
  redshift        (De)activates redshift mode.
Brightness values are truncated between 15 and 100 to avoid an unresponsive black screen. 
"""
from docopt import docopt
import sys, subprocess, os
from os.path import abspath, dirname, join as path_join
from subprocess import run, PIPE, CalledProcessError
import json

# Get the current path:
HERE = dirname(abspath(__file__))
json_file = path_join(HERE, '.brightness.json')

def load_json(fname):
    with open(fname) as f: obj = json.load(f)
    return obj

def save_json(obj, fname):
    with open(fname, 'w') as f: json.dump(obj, f)
    return

def load_settings():
    try: settings = load_json(json_file)
    except: settings = {'brightness': 100, 'redshift':False} ; save_json(settings, json_file)
    return settings

def save_settings(settings):
    return save_json(settings, json_file)

def apply_settings(brightness, redshift):
    p = brightness / 100.
    if redshift:
        run(['redshift', '-o', '-b', f'{p}:{p}'])
    else:
        # Get primary display:
        out = run('xrandr', stdout=PIPE).stdout.decode()
        displays = [l[0] for l in [l.split() for l in out.split('\n')] if len(l)>1 and l[1]=='connected']
        # Set y as the brightness:
        for display in displays:
            print(f"Setting brightness of {display} to {brightness}%")
            run(['xrandr', '--output', f'{display}', '--brightness', f'{p}'])
    return

if __name__ == '__main__':
    args = docopt(__doc__)
    if args['get']:
        settings = load_settings()
        print(settings['brightness'])
    elif args['set'] or args['increase'] or args['decrease']:
        settings = load_settings()
        value = int(args['<value>'])
        previous = int(settings['brightness'])
        if args['set']: brightness = value
        elif args['increase']: brightness = previous + value
        elif args['decrease']: brightness = previous - value
        brightness = min(100,max(15, brightness))
        settings['brightness'] = brightness
        apply_settings(**settings)
        save_settings(settings)
    elif args['redshift']:
        settings = load_settings()
        settings['redshift'] = not settings['redshift']
        apply_settings(**settings)
        save_settings(settings)
