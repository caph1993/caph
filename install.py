#!/usr/bin/python3
import os, sys, time
from subprocess import run, DEVNULL, PIPE


thisfile = os.path.abspath(__file__)
thisdir = os.path.dirname(thisfile)
homedir = os.getenv("HOME")

pydir = os.path.join(thisdir, "python_modules")
bindir = os.path.join(thisdir, "bin")

tstamp = f' # {time.ctime()} by {thisfile}'

def upsert_entry(lines, preffix, replace):
    wh = [i for i,l in enumerate(lines) if l.startswith(preffix)]
    for i in wh: lines[i] = replace
    if not wh: lines.append(replace)
    return

rcfiles = ['.zshrc', '.bashrc']
rcfiles = [os.path.join(homedir, rc) for rc in rcfiles]
rcfiles = [rc for rc in rcfiles if os.path.isfile(rc)]

assert rcfiles

for rcfile in rcfiles:
    p = run(['cat', rcfile], stdout=PIPE)
    lines = p.stdout.decode().split('\n')
    while lines and not lines[-1]: lines.pop()

    pre = "export CAPH_PY="
    tgt = f'export CAPH_PY="{pydir}"'+tstamp
    upsert_entry(lines, pre, tgt)

    pre = 'export PYTHONPATH="${PYTHONPATH}:${CAPH_PY}"'
    tgt = 'export PYTHONPATH="${PYTHONPATH}:${CAPH_PY}"'+tstamp
    upsert_entry(lines, pre, tgt)

    pre = 'export JUPYTER_PATH="${CAPH_PY}:${JUPYTER_PATH}"'
    tgt = 'export JUPYTER_PATH="${CAPH_PY}:${JUPYTER_PATH}"'+tstamp
    upsert_entry(lines, pre, tgt)

    pre = "export CAPH_BIN="
    tgt = f'export CAPH_BIN="{bindir}"'+tstamp
    upsert_entry(lines, pre, tgt)

    pre = 'export PATH="${PATH}:${CAPH_BIN}"'
    tgt = 'export PATH="${PATH}:${CAPH_BIN}"'+tstamp
    upsert_entry(lines, pre, tgt)

    while lines and not lines[-1]: lines.pop()
    lines.append('')

    with open(rcfile, "w") as f:
        f.write('\n'.join(lines))

