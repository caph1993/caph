#    1. Install:
sudo cp /home/carlos/src/custom-startup/template /etc/systemd/system/custom-startup.service
ln -s /etc/systemd/system/custom-startup.service /home/carlos/src/custom-startup/service
sudo systemctl daemon-reload
    
#    2. Edit the service using the custom-startup.service symbolic-link:

gedit /home/carlos/src/custom-startup/service

#    Start / stop / status:

sudo systemctl start custom-startup
sudo systemctl status custom-startup # Check printed messages
#sudo systemctl stop custom-startup # Does not actually make sense

#    Enable at boot / disable:

sudo systemctl enable custom-startup
#sudo systemctl disable custom-startup
    

