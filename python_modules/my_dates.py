import holidays
from datetime import date, timedelta


class MyDate():
    co_holidays = holidays.Colombia()
    
    def __init__(self, numdate):
        if isinstance(numdate, date):
            d = numdate
        elif isinstance(numdate, MyDate):
            d = numdate.date
        else:
            if isinstance(numdate, str):
                numdate = int(''.join([c for c in numdate if c.isdigit()]))
            assert isinstance(numdate, int)
            year = numdate//10000
            month = (numdate//100)%100
            day = numdate%100
            d = date(year, month, day)
        self.date = d

    def __int__(self):
        d = self.date
        return d.year*10000+d.month*100+d.day
    
    def weekday(self):
        '''0 is monday. 6 is sunday'''
        return self.date.weekday()
    
    def __getattr__(self, name):
        return getattr(self.date, name)
    
    def __add__(self, ndays):
        d = self.date + timedelta(days=ndays)
        return MyDate(d.year*10000+d.month*100+d.day)
    def __radd__(self, ndays):
        return self + ndays
    def __sub__(self, ndays):
        if isinstance(ndays, int): return self + (-ndays)
        elif isinstance(ndays, date): return (self.date-ndays).ndays
        elif isinstance(ndays, MyDate): return (self.date-ndays.date).days
        else: raise Exception(f'Can not subtract «{type(ndays)}» type from MyDate')

    def __rsub__(self, ndays):
        return self - ndays
    def __str__(self):
        return str(int(self))
    def __repr__(self):
        return 'MyDate({})'.format(str(self))

    def is_holiday(self):
        return self.date in self.co_holidays

    def is_free(self):
        ans = self.weekday() in (5,6)
        ans = ans or self.is_holiday()
        ans = ans or (self.date.month, self.date.day) in [
            (10,31), (12, 24), (12, 30), (12,26), (1, 1)
        ]
        return ans

    def non_meat(self):
        s = self.co_holidays.get(self.date) or ''
        s = s.lower()
        ans = s.startswith('jueves santo')
        ans = ans or s.startswith('viernes santo')
        return ans

    def eq(self):
        ''' «Equivalent weekday»
        5 is saturday-eq:  free and tomorrow free
        6 is sunday-eq:    free and tomorrow work
        4 is friday-eq:    work and tomorrow free
        0 is monday-eq:    work and yesterday free
        1 is tuesday-eq:   work and yesterday monday-eq
        3 is thursday-eq:  work and tomorrow friday-eq
        2 is wednesday-eq: otherwise
        * Special cases around non-meat days
        '''
        tomorrow = self + 1
        if self.non_meat(): day = 0
        elif (self-1).non_meat(): day = 1
        elif self.is_free() and tomorrow.is_free(): day = 5
        elif self.is_free() and not tomorrow.is_free(): day = 6
        elif tomorrow.is_free(): day = 4
        elif (self-1).is_free(): day = 0
        elif (self+2).is_free(): day = 3
        elif (self-2).is_free(): day = 1
        else: day = 2
        return day

    @staticmethod
    def range(date_from, date_to, as_ints=False):
        a, b = MyDate(date_from), MyDate(date_to)
        R = [a+k for k in range(1+(b-a))]
        if as_ints: R = [int(d) for d in R]
        return R



        