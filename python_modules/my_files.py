import os, tempfile
from tqdm import tqdm


class MyFileReader:
    def __init__(self, filename, nlines=None):
        self.filename = filename
        self.n = nlines

    @staticmethod
    def count_lines(filename, skip=0):
        with open(filename) as f:
            n = -skip
            while len(f.readline()):
                n+=1
        return max(0,n)

    @staticmethod
    def count_lines_faster(filename):
        def chunks(reader):
            b = reader(1024 * 1024)
            while b: yield b ; b = reader(1024*1024)
        with open(filename, 'rb') as f:
            n = sum(buf.count(b'\n') for buf in chunks(f.raw.read))
        return n

    def __iter__(self):
        if self.n==None:
            self.n = self.count_lines_faster(self.filename)
        with open(self.filename) as f, tqdm(range(self.n)) as pbar:
            for _ in pbar:
                yield f.readline().strip()
        return


def tmp_dir(*args, **kw):
    return tempfile.TemporaryDirectory(*args, **kw)


def tmp_file(*args, **kw):
    return tempfile.NamedTemporaryFile(*args, **kw)


def grant_access(filename):
    # Grant permissions on the file to any other user (psql):
    current_umask = os.umask(0)
    os.umask(current_umask)
    os.chmod(filename, 0o666 & ~current_umask)
    return
        