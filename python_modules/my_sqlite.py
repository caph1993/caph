import os, sqlite3
import numpy as np # just support for numpy integers
from .my_common_sql import MyCommonSQL

sqlite3.register_adapter(np.int64, lambda val: int(val))


class MySQLite(MyCommonSQL):
    symbol = '?'
    base_exception = sqlite3.Error

    def __init__(self, fname, must_exist=False):
        if must_exist:
            assert os.path.isfile(fname), f'Required db file not found «{fname}»'
        self.conn = sqlite3.connect(fname, check_same_thread=False)
        self.locked = False
    
    def commit(self):
        try:
            self.wait_for_lock()
            self.conn.commit()
        finally:
            self.locked = False

    def _iter_exec(self, query, *args, header):
        '''Query output generator'''
        cur = self.conn.cursor()
        try:
            try:
                self.wait_for_lock()
                cur.execute(query, args)
            finally:
                self.locked = False
            if cur.description:
                if header:
                    yield [x[0] for x in cur.description]
                yield from map(list, cur)
        except sqlite3.Error as e:
            self.conn.rollback()
            err = e.args[0]
            msg = f"{err}\nQuery:\n{query}\nArguments:\n{args}\n"
            msg += f"Error: «{err}»\n"+"-"*20
            e.args = (msg, *e.args[1:])
            raise
        except:
            self.conn.rollback()
            raise
        return

    def wait_for_lock(self):
        while self.locked:
            time.sleep(0.01)
            print('db is locked...', flush=True)
        self.locked = True
        return

    def upsert_rows(self, table, rows):
        cols = self.columns(table)
        self.upsert_dicts((dict(zip(cols, row)) for row in rows))

    def upsert_row(self, table, row):
        return self.upsert_rows(table, [row])

    def upsert_dicts(self, table, dicts):
        cols = self.columns(table)
        set_cols = set(cols)
        for dict_ in dicts:
            self._check_keys(dict_, set_cols)
            try:
                values = [dict_.get(k, None) for k in cols]
                self._insert_row(table, values)
            except sqlite3.IntegrityError as e:
                self._update_dict(table, dict_)
        self.commit()

    def upsert_dict(self, table, dict_):
        self.upsert_dicts(table, [dict_])
        return

    def _unique_constraint_columns(self, table):
        idxs = self.dicts(f"pragma index_list('{table}')")
        col_names = []
        for idx in idxs:
            idx_name = idx['name']
            cols = self.dicts(f"PRAGMA index_info('{idx_name}')")
            col_names.extend([x['name'] for x in cols])
        return col_names

    def insert_dict(self, table, dict_):
        return self.insert_dicts(table, [dict_])

    def replace_rows(self, table, rows):
        cols = self.columns(table)
        cols_str = ','.join(cols)
        fmt = ','.join('?' for x in cols)
        for row in rows:
            self._exec(f"REPLACE INTO {table} ({cols_str}) VALUES ({fmt})", *row)
        self.commit()

    def replace_row(self, table, row):
        return self.replace_rows(table, [row])

    def replace_dicts(self, table, dicts):
        set_cols = set(self.columns(table))
        for dict_ in dicts:
            keys, vals = zip(*dict_.items())
            self._check_keys(keys, set_cols)
            fmt = ','.join('?' for x in keys)
            join_keys = ','.join(keys)
            self._exec(f"REPLACE INTO {table} ({join_keys}) VALUES ({fmt})", *vals)
        self.commit()

    def replace_dict(self, table, dict_):
        return self.replace_dicts(table, [dict_])
