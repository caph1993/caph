
class MyCommonSQL():
    symbol = '?'
    base_exception = Exception

    def __init__(self, conn):
        self.conn = conn

    def __enter__(self):
        return self
    
    def __exit__(self, *args):
        self.close()

    def close(self):
        self.conn.close()
    
    def commit(self):
        self.conn.commit()

    def exec(self, query, *args):
        self._exec(query, *args)
        self.commit()

    def _exec(self, query, *args):
        '''Execute query without committing'''
        for just_consume in self._iter_exec(query, *args, header=False):
            pass

    def _iter_exec(self, query, *args, header):
        '''Query output generator'''
        cur = self.conn.cursor()
        query = query.replace('?', self.symbol)
        try:
            cur.execute(query, args)
        except Exception as e:
            try: self.conn.rollback()
            except: pass
            summary = self._summary(self, query, *args)
            raise Exception(summary) from e
        if cur.description:
            if header: yield [x[0] for x in cur.description]
            yield from map(list, cur)
        cur.close()
        return


    def _custom_exception(self, query, *args):
        qmsg = '\n'.join(f'   {x}' for x in query.split('\n'))
        amsg = '\n'.join(f'   {x}' for x in (args or ['(no arguments)']))
        return f"{e}\nQuery:\n{qmsg}\nQuery arguments:\n{amsg}"


    def iter_rows(self, query, *args, header=False):
        yield from self._iter_exec(query, *args, header=header)

    def iter_dicts(self, query, *args):
        gen = self.iter_rows(query, *args, header=True)
        header = next(gen)
        yield from (dict(zip(header, row)) for row in gen)

    def rows(self, query, *args, header=False):
        return list(self.iter_rows(query, *args, header=header))

    def rowsT(self, query, *args, header=False):
        rows = list(self.iter_rows(query, *args, header=True)) # with header in case empty
        R = len(rows)
        C = len(rows[0]) if len(rows) else 0
        R0 = 0 if header else 1
        return [[rows[i][j] for i in range(R0, R)] for j in range(C)]

    def dicts(self, query, *args):
        return list(self.iter_dicts(query, *args))

    def one_dict(self, query, *args, no_errors=False):
        ret = None
        try: ret = self.dicts(query, *args)[0]
        except IndexError:
            if not no_errors: raise
        return ret

    def one_row(self, query, *args, no_errors=False):
        ret = None
        try: ret = self.rows(query, *args, header=False)[0]
        except IndexError:
            if not no_errors: raise
        return ret

    def one_value(self, query, *args, no_errors=False):
        ret = None
        try: ret = self.rows(query, *args, header=False)[0][0]
        except IndexError:
            if not no_errors: raise
        return ret

    def one_column(self, query, *args):
        query_table = self.iter_rows(query, *args)
        next(query_table)
        return [row[0] for row in query_table]

    def columns(self, table):
        return self.rows(f'SELECT * FROM {table} WHERE 1=0 LIMIT 0')[0]

    def nrows(self, table):
        return self.rows(f'SELECT count(*) FROM {table}')[1][0]

    def insert_rows(self, table, rows, chunk_size=1000):
        cols = self.columns(table)
        for i,row in enumerate(rows):
            self._insert_row(table, row)
            if i+1==chunk_size: self.commit()
        self.commit()

    def insert_row(self, table, row):
        return self.insert_rows(table, [row])

    def insert_dicts(self, table, dicts, chunk_size=1000):
        cols = self.columns(table)
        rows = ([dict_.get(k, None) for k in cols] for dict_ in dicts if self._check_keys(dict_, cols))
        return self.insert_rows(table, rows)

    def insert_dict(self, table, dict_):
        return self.insert_dicts(table, [dict_])

    def _insert_row(self, table, row):
        fmt = ','.join('?' for x in row)
        query = f"INSERT INTO {table} VALUES ({fmt})"
        return self._exec(query, *row)

    def _update_dict(self, table, dict_, unique=None):
        keys = [k for k in dict_]
        values = [dict_[k] for k in keys]
        unique = unique or self._unique_constraint_columns(table)
        equals = ",".join(f"{k}=?" for k in keys if k not in unique)
        where = " AND ".join(f"{k}=?" for k in keys if k in unique)
        values1 = [v for k,v in zip(keys, values) if k not in unique]
        values2 = [v for k,v in zip(keys, values) if k in unique]
        values = values1+values2
        query = f"UPDATE {table} SET {equals} WHERE {where}"
        return self._exec(query, *values)

    def _unique_constraint_columns(self, table):
        raise NotImplementedError

    def _check_keys(self, keys, set_cols):
        for k in keys:
            assert k in set_cols, f'Key «{k}» not present in table'
        return True
