import time

class  MyTimer():
    def __enter__(self): self.t = time.time()
    def __exit__(self,*_):
        t = time.time()
        print('... Elapsed {:.3f} seconds'.format(t-self.t))


class MyRandom():
    @staticmethod
    def alphas(n=15):
        return ''.join(random.sample('abcdefghijklmnopqrstuvwxyz', n))


class MyHash():
    @staticmethod
    def sha1(s):
        return hashlib.sha1(s.encode('utf-8')).hexdigest()
    @staticmethod
    def sha256(s):
        return hashlib.sha256(s.encode('utf-8')).hexdigest()
    @staticmethod
    def sha512(s):
        return hashlib.sha512(s.encode('utf-8')).hexdigest()
    @staticmethod
    def md5(s):
        return hashlib.md5(s.encode('utf-8')).hexdigest()



def my_bokeh_plot(lines, **kwargs):
    def_kwargs = dict(
        width=750, height=320, active_scroll="wheel_zoom",
        title=None, x_axis_label='x', y_axis_label='y',
    )
    def_kwargs.update(kwargs)
    p = bokeh_figure(**def_kwargs)
    palette = iter(Category10[10])
    for l in lines:
        l = {**l}
        x, y = l.pop('x'), l.pop('y')
        if 'line_color'  not in l: l['line_color'] = next(palette)
        if 'line_width'  not in l: l['line_width'] = 1
        if 'muted_alpha' not in l: l['muted_alpha'] = 0
        p.line(x, y, **l)
    if p.legend:
        p.legend.click_policy="mute"
    bokeh_show(p)
    return



        