import os
from configparser import ConfigParser
import numpy as np # just support for numpy integers
import psycopg2, psycopg2.extras
from .my_common_sql import MyCommonSQL


psycopg2.extensions.register_adapter(np.int64, psycopg2._psycopg.AsIs)
psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
psycopg2.extensions.register_type(psycopg2.extensions.new_type(
    psycopg2.extensions.DECIMAL.values, 'DEC2FLOAT',
    lambda value, curs: float(value) if value is not None else None)
)

class MyPostgres(MyCommonSQL):
    symbol = '%s'
    base_exception = psycopg2.IntegrityError

    def __init__(self, config_ini, section='postgresql'):
        parser = ConfigParser()
        parser.read(config_ini)
        if parser.has_section(section):
            config = {x[0]: x[1] for x in parser.items(section)}
        else:
            raise Exception(f'Section «{section}» not found in the config file')
        self.conn = psycopg2.connect(**config)
        return

    def _unique_constraint_columns(self, table):
        data = self.dicts("""SELECT UNNEST(conkey) as i FROM pg_constraint
            WHERE conrelid = '{}'::regclass::oid;""".format(table)
        )
        idxs = [row['i']-1 for row in data]
        columns = self.columns(table)
        columns = [columns[i] for i in idxs]
        return columns

    def upsert_dicts(self, table, dicts):
        cols = self.columns(table)
        unique = self._unique_constraint_columns(table)
        set_cols = set(cols)
        for dict_ in dicts:
            self._check_keys(dict_, set_cols)
            try:
                values = [dict_.get(k, None) for k in cols]
                self._insert_row(table, values)
            except Exception as e:
                #print("HERE I AM \n\n\n", e)
                #if not hasattr(e, 'pgocde') or (e.pgcode!='23505' and e.pgcode!='23502'): raise e
                #print("HERE I AM \n\n\n", e)
                self._update_dict(table, dict_, unique)
        self.commit()


    def update_rows(self, table, rows, columns=None):
        columns = columns or self.columns(table)
        unique = self._unique_constraint_columns(table)
        q_set = ",".join(f"{k}=?" for k in columns if k not in unique)
        q_where = " AND ".join(f"{k}=?" for k in columns if k in unique)
        idx_set = [i for i,k in enumerate(columns) if k not in unique]
        idx_where = [i for i,k in enumerate(columns) if k in unique]
        query = f"UPDATE {table} SET {q_set} WHERE {q_where}"

        for row in rows:
            assert len(row)==len(columns)
            v_set = [row[i] for i in idx_set]
            v_where = [row[i] for i in idx_where]
            self._exec(query, *v_set, *v_where)
        self.commit()


    def upsert_rows(self, table, rows, columns=None):
        table_columns = self.columns(table)
        columns = columns or table_columns
        unique = self._unique_constraint_columns(table)
        q_values = ",".join("?" if k in columns else "NULL" for k in table_columns)
        q_set = ",".join(f"{k}=?" for k in columns if k not in unique)
        q_where = " AND ".join(f"{k}=?" for k in columns if k in unique)
        q_conflict = ",".join(k for k in columns if k in unique)
        idx_set = [i for i,k in enumerate(columns) if k not in unique]
        idx_where = [i for i,k in enumerate(columns) if k in unique]
        query = f"""
            INSERT INTO {table} VALUES ({q_values})
            ON CONFLICT ({q_conflict}) DO UPDATE SET {q_set}
        """
        for row in rows:
            assert len(row)==len(columns), f'Assertion failed: len({row})==len({columns})'
            v_set = [row[i] for i in idx_set]
            v_where = [row[i] for i in idx_where]
            self._exec(query, *row, *v_set)
        self.commit()


    def upsert_dicts(self, table, dicts):
        if not dicts: return
        def row_gen(dicts):
            cols0 = None
            for dict_ in dicts:
                cols = sorted(dict_.keys())
                assert cols0==None or cols0==cols, 'MyPostgres.upsert_dicts does not support dictionaries having different keys'
                if cols0==None:
                    cols0 = cols
                    yield cols
                yield [dict_[k] for k in cols]
            return
        rows = row_gen(dicts)
        columns = next(rows)
        return self.upsert_rows(table, rows, columns)
