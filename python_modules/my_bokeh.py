from bokeh.plotting import figure as bokeh_figure, show as bokeh_show
from bokeh.palettes import Category10, Category20
from bokeh.io import output_notebook, output_file, reset_output # for external import
from math import isfinite


def my_bokeh_plot(lines,
        width=750, height=320, title="",
        x_axis_label='x', y_axis_label='y',
        palette=None,
        active_scroll="wheel_zoom",
        zero=True, auto_legend=True,
        show=True,
        **figure_kwargs):
    p = bokeh_figure(
        width=width, height=height, title=title,
        active_scroll=active_scroll,
        x_axis_label=x_axis_label, y_axis_label=y_axis_label,
        **figure_kwargs
    )
    auto_legends = auto_legend and len(lines)>1 and (f'Curve {i+1}' for i in range(len(lines)))
    palette = iter(palette or (Category10[10] if len(lines)<=10 else Category20[20]))
    copies = [{**args} for args in lines]
    for args in copies:
        color = args.pop('color', None) or next(palette) # Only cycle if not present
        scatter = args.pop('scatter', False)
        marker = args.pop('marker', scatter and 'x')
        legend = args.pop('legend', auto_legends and next(auto_legends))
        x = args.pop('x')
        y = args.pop('y')
        if not hasattr(y, '__len__'): # support for scalars
            y = [y for _ in x]
        assert len(x)==len(y), (len(x), len(y))
        line_kwargs = dict(
            line_width=1,
            muted_alpha=0,
            line_alpha=1,
        )
        line_kwargs.update(args)
        if not scatter:
            p.line(x, y, legend=legend, line_color=color, **line_kwargs)
        if marker:
            p.scatter(x, y, legend=legend, marker=marker, line_color=color, color=color, **line_kwargs)
    if zero:
        x_samples = [v for l in lines for v in l['x'] if isfinite(v)]
        p.line([min(x_samples), max(x_samples)], [0, 0], line_color='black', line_width=1, line_alpha=0.8)
    if p.legend:
        p.legend.click_policy="mute"
    return bokeh_show(p) if show else p
        